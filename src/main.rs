use itertools::Itertools;
use man::prelude::*;

use std::convert::TryFrom;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, Result};
use clap::{AnyArg, App, ArgSettings};

#[cfg(any(feature = "sqop", feature = "keyring-linter"))]
use structopt::StructOpt;

#[cfg(any(feature = "sqop", feature = "sq"))]
use cargo_lock::Lockfile;

mod manpage_maker;

#[cfg(feature = "sq")]
#[allow(dead_code)]
mod sq_cli;

#[cfg(feature = "sqop")]
#[allow(dead_code)]
mod sop_cli;

#[cfg(feature = "sqv")]
#[allow(dead_code)]
mod sqv_cli;

#[cfg(feature = "keyring-linter")]
#[allow(dead_code)]
mod keyring_linter_cli;

fn main() -> anyhow::Result<()> {
    #[cfg(feature = "keyring-linter")]
    keyring_linter()?;
    #[cfg(feature = "sq")]
    sq()?;
    #[cfg(feature = "sqv")]
    sqv()?;
    #[cfg(feature = "sqop")]
    sqop()?;
    Ok(())
}

#[cfg(feature = "sq")]
fn sq() -> anyhow::Result<()> {
    for &net in &[false, true] {
        for &autocrypt in &[false, true] {
            let dir = PathBuf::from(format!(
                "man-sq{}{}",
                if net { "-net" } else { "" },
                if autocrypt { "-autocrypt" } else { "" },
            ));
            let _ = std::fs::remove_dir_all(&dir);
            std::fs::create_dir(&dir)?;

            let data = manpage_maker::ManpageMaker::new(
                sq_cli::configure(App::new("sq"), net, autocrypt),
                std::path::Path::new("src/sq-Cargo.toml"),
                Some("sequoia-sq".to_owned()),
            )?;
            let backend_version = Lockfile::load("src/sq-Cargo.lock")?
                .packages
                .iter()
                .find(|p| p.name.as_str() == "sequoia-openpgp")
                .map(|p| p.version.to_string())
                .expect("no sequoia-openpgp in Cargo.lock");
            let version = format!("{} (sequoia-openpgp {})",
                                  data.metadata.version,
                                  backend_version);

            let manpages = generate(data.app.clone(), Some(&version))?;

            let manpages = manpages.map(|manpage| {
                manpage.see_also("For the full documentation see <https://docs.sequoia-pgp.org/sq/>.".to_owned())
            });
            let manpages = add_related_see_also(manpages.collect::<Vec<_>>());

            for mut manpage in manpages {
                manpage = add_authors(data.metadata.authors.clone(), manpage);
                write_manpage(&dir, manpage)?
            }
        }
    }
    Ok(())
}

#[cfg(feature = "sqop")]
fn sqop() -> anyhow::Result<()> {
    let dir = PathBuf::from("man-sqop");
    let _ = std::fs::remove_dir_all(&dir);
    std::fs::create_dir(&dir)?;

    // Change the app name to sqop.
    let mut app = sop_cli::SOP::clap();
    app = app.name("sqop");
    let data = manpage_maker::ManpageMaker::new(
        app,
        std::path::Path::new("src/sop-Cargo.toml"),
        Some("sequoia-sop".to_owned()),
    )?;

    let frontend_version = data.metadata.version;
    let backend_version = Lockfile::load("src/sop-Cargo.lock")?
        .packages
        .iter()
        .find(|p| p.name.as_str() == "sequoia-openpgp")
        .map(|p| p.version.to_string())
        .expect("no sequoia-openpgp in Cargo.lock");
    let version = format!("{}, using sequoia {}", frontend_version, backend_version);

    let manpages = generate(data.app.clone(), Some(&version))?;
    let manpages = add_related_see_also(manpages.collect::<Vec<_>>());

    for mut manpage in manpages {
        manpage = add_authors(data.metadata.authors.clone(), manpage);
        write_manpage(&dir, manpage)?
    }

    Ok(())
}

#[cfg(feature = "sqv")]
fn sqv() -> anyhow::Result<()> {
    let dir = PathBuf::from("man-sqv");
    let _ = std::fs::remove_dir_all(&dir);
    std::fs::create_dir(&dir)?;

    let data = manpage_maker::ManpageMaker::new(
        sqv_cli::build(),
        std::path::Path::new("src/sqv-Cargo.toml"),
        Some("sequoia-sqv".to_owned()),
    )?;
    let version = data.metadata.version;
    let manpages = generate(data.app.clone(), Some(&version))?;

    for mut manpage in manpages {
        manpage = add_authors(data.metadata.authors.clone(), manpage);
        if let Some(ref documentation) = data.metadata.documentation {
            manpage = manpage.see_also(format!("For documentation, see <{}>.", documentation));
        };
        if let Some(ref homepage) = data.metadata.homepage {
            manpage = manpage.see_also(format!("For the project's homepage, see <{}>.", homepage));
        };
        write_manpage(&dir, manpage)?
    }

    Ok(())
}

#[cfg(feature = "keyring-linter")]
fn keyring_linter() -> anyhow::Result<()> {
    let dir = PathBuf::from("man-keyring-linter");
    let _ = std::fs::remove_dir_all(&dir);
    std::fs::create_dir(&dir)?;

    let data = manpage_maker::ManpageMaker::new(
        keyring_linter_cli::Linter::clap(),
        std::path::Path::new("src/keyring-linter-Cargo.toml"),
        Some("sequoia-keyring-linter".to_owned()),
    )?;
    let version = data.metadata.version;
    let manpages = generate(data.app.clone(), Some(&version))?;

    for mut manpage in manpages {
        manpage = add_authors(data.metadata.authors.clone(), manpage);
        write_manpage(&dir, manpage)?
    }

    Ok(())
}

fn add_related_see_also(manpages: Vec<Manual>) -> impl Iterator<Item = Manual> {
    // TODO do this properly, with less string magic
    //
    // Finds which manpages to list in the see also section:
    //   1. The main command
    //   2. All second-level subcommands
    //   3. Only sibling third-level subcommands (same second-level)
    fn find_related(name: &str, mut candidates: Vec<String>) -> Vec<String> {
        //reconstruct structure
        let top_level = candidates
            .iter()
            .position(|name| {
                name == "sq" || name == "sq-keyring-linter" || name == "sqop"
            })
            .unwrap();
        let top_level = candidates.remove(top_level);
        let (second_level, third_level): (Vec<String>, Vec<String>) =
            candidates.iter().cloned().partition(|name| {
                name.split(' ').collect::<Vec<&str>>().len() == 2
            });

        //Always include first and second level commands
        let mut output = second_level.clone();
        output.push(top_level);

        let subcommand: Option<String> =
            second_level.iter().cloned().find(|sl| name.contains(sl));
        if let Some(sc) = subcommand {
            let mut tl = third_level
                .into_iter()
                .filter(|tl| tl.contains(&sc))
                .collect::<Vec<String>>();
            output.append(&mut tl);
        }
        output.sort_unstable();
        output.dedup();
        output
    }

    let manpage_names = manpages
        .iter()
        .map(|man| man.name.clone())
        .collect::<Vec<String>>();
    let related = manpage_names
        .clone()
        .iter()
        .cloned()
        .map(|man| find_related(&man, manpage_names.clone()))
        .collect::<Vec<Vec<String>>>();
    let man_plus_related = manpages.into_iter().zip(related);

    // Add related manpages to see also sections
    let manpages = man_plus_related.map(|(manpage, related)| {
        let related = related
            .iter()
            .map(|related| [&related.replace(" ", "-"), "(1)"].join(""))
            .collect::<Vec<String>>()
            .join(", ");

        // don't justify and don't hyphenate
        manpage.see_also([".ad l\n.nh\n", &related].join(""))
    });

    manpages
}

fn generate(
    app: App<'static, 'static>,
    version: Option<&str>,
) -> anyhow::Result<impl Iterator<Item = Manual>> {
    let mut manpages = Vec::new();
    create_manpage(app.clone(), None, version, &mut manpages);

    Ok(manpages.into_iter())
}

fn create_manpage(
    app: clap::App,
    parent: Option<&str>,
    version: Option<&str>,
    manpages: &mut Vec<Manual>,
) {
    let name = match parent {
        Some(parent) => [parent, &app.p.meta.name].join(" "),
        None => app.p.meta.name,
    };
    let mut manpage = Manual::new(&name);
    manpage = manpage.date(date_today_iso());
    manpage = add_help_flag(manpage);
    if parent.is_none() {
        manpage = add_version_flag(manpage);
    }

    if let Some(about) = app
        .p
        .meta
        .long_about
        .filter(|la| !la.is_empty())
        .or(app.p.meta.about)
    {
        manpage = manpage.about(about);
    };
    for flag in app.p.flags {
        let mut man_flag = Flag::new();
        if let Some(short) = flag.short() {
            man_flag = man_flag.short(&format!("-{}", short));
        }
        if let Some(long) = flag.long() {
            man_flag = man_flag.long(&format!("--{}", long));
        }
        if let Some(help) = flag.long_help().or_else(|| flag.help()) {
            man_flag = man_flag.help(help);
        }
        manpage = manpage.flag(man_flag);
    }
    for option in app.p.opts {
        // prefer val_name over name. why though?
        let name = option.val_names().map_or(option.name(), |vn| vn[0]);
        let mut man_option = Opt::new(name);
        if let Some(short) = option.short() {
            man_option = man_option.short(&format!("-{}", short));
        }
        if let Some(long) = option.long() {
            man_option = man_option.long(&format!("--{}", long));
        }
        if let Some(help) = option.long_help().or(option.help()) {
            let possible_values =
                option.possible_vals().map_or("".into(), |pvs| {
                    ["  [possible values: ", &pvs.join(", "), "]"].join("")
                });
            let default_value = option.default_val().map_or("".into(), |dv| {
                ["  [default: ", &dv.to_string_lossy(), "]"].join("")
            });
            let help = [help, &default_value, &possible_values].join("");
            man_option = man_option.help(&help);
        }
        manpage = manpage.option(man_option);
    }
    for arg in app.p.positionals {
        // TODO don't disregard count
        //arg is a pair of (count, Arg)
        let arg = arg.1;
        // Prefer val_name over name, if it exists
        let name = arg.val_names().map_or(arg.name(), |vn| vn[0]);
        let required = arg.is_set(ArgSettings::Required);
        let mut man_arg = man::Arg::new(name, required);
        if let Some(help) = arg.long_help().or(arg.help()) {
            man_arg = man_arg.description(help);
        }
        manpage = manpage.arg(man_arg);
    }
    if !app.p.subcommands.is_empty() {
        manpage = add_help_subcommand(manpage);
    };
    for subcommand in app.p.subcommands.clone() {
        let sc_meta = subcommand.p.meta;
        let mut man_subcommand = Subcommand::new(&sc_meta.name);
        if let Some(about) = sc_meta
            .long_about
            .filter(|la| !la.is_empty())
            .or(sc_meta.about)
        {
            man_subcommand = man_subcommand.description(about.trim());
        };
        manpage = manpage.subcommand(man_subcommand);
    }

    // this is specific to sequoia
    if let Some(more_help) = app.p.meta.more_help {
        manpage = parse_more_help(more_help, manpage);
    }

    let version = version.or(app.p.meta.version);
    if let Some(version) = version {
        manpage = manpage.version(version);
    };

    for subcommand in app.p.subcommands {
        create_manpage(subcommand.clone(), Some(&name), version, manpages);
    }

    manpages.push(manpage);
}

#[derive(Debug)]
enum AfterHelpSectionHeader {
    ExitStatus,
    Example,
    SeeAlso,
}

impl std::fmt::Display for AfterHelpSectionHeader {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AfterHelpSectionHeader::Example => write!(f, "EXAMPLE"),
            AfterHelpSectionHeader::ExitStatus => write!(f, "EXIT STATUS"),
            AfterHelpSectionHeader::SeeAlso => write!(f, "SEE ALSO"),
        }
    }
}

impl std::convert::TryFrom<&str> for AfterHelpSectionHeader {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "EXAMPLES:" | "EXAMPLE:" => Ok(AfterHelpSectionHeader::Example),
            "EXIT STATUS:" => Ok(AfterHelpSectionHeader::ExitStatus),
            "SEE ALSO:" => Ok(AfterHelpSectionHeader::SeeAlso),
            v => Err(anyhow!("{} is not an expected section header", v)),
        }
    }
}

impl AfterHelpSectionHeader {
    fn is_heading(value: &str) -> bool {
        Self::try_from(value).is_ok()
    }
}

// TODO respect see also paragraphs
fn parse_more_help(more_help: &str, mut manpage: Manual) -> Manual {
    let mut more_help_iter = more_help.lines().peekable();
    while more_help_iter.peek().is_some() {
        let first_line = more_help_iter
            .peek()
            .and_then(|&line| AfterHelpSectionHeader::try_from(line).ok());

        // Skip the heading and empty lines
        loop {
            match more_help_iter.peek() {
                Some(&foo) => {
                    if foo.is_empty() || AfterHelpSectionHeader::is_heading(foo)
                    {
                        more_help_iter.next();
                    } else {
                        break;
                    };
                }
                None => break,
            };
        }

        // Read lines up to the following header
        let lines = more_help_iter
            .peeking_take_while(|line| {
                !AfterHelpSectionHeader::is_heading(line)
            })
            .map(|line| line.to_owned())
            .collect::<Vec<String>>();

        if !lines.is_empty() {
            match first_line {
                Some(AfterHelpSectionHeader::Example) => {
                    manpage = add_examples(manpage, lines);
                }
                Some(AfterHelpSectionHeader::ExitStatus) => {
                    let section = man::Section::new("EXIT STATUS:")
                        .paragraph(&lines.join("\n"));
                    manpage = manpage.custom(section);
                }
                Some(AfterHelpSectionHeader::SeeAlso) => {
                    manpage = manpage.see_also(lines.join("\n"));
                }
                None => {
                    manpage = manpage.description(lines.join("\n"));
                }
            }
        }
    }
    manpage
}

fn add_authors(authors: Vec<String>, mut manpage: Manual) -> Manual {
    for author in authors.iter() {
        let mut split = author.split(" <");
        let name = split.next().unwrap();
        let email = split.next().unwrap().replace(">", "");
        let author = Author::new(name).email(&email);
        manpage = manpage.author(author);
    }
    manpage
}

/// Parse examples from clap's after_help (called more_help internally)
fn add_examples(mut manpage: Manual, more_help: Vec<String>) -> Manual {
    // jump over EXAMPLE and possible subsequent newlines
    let mut more_help_iter = more_help
        .into_iter()
        .skip_while(|line| line.is_empty() || line.contains("EXAMPLE"));

    while let Some(example) = get_next_example(&mut more_help_iter) {
        manpage = manpage.example(example);
    }

    fn get_next_example<U>(mut iter: U) -> Option<Example>
    where
        U: std::iter::Iterator<Item = String>,
    {
        let example_lines = iter
            .by_ref()
            .take_while(|line| !line.is_empty())
            .collect::<Vec<_>>();
        if example_lines.is_empty() {
            None
        } else {
            Some(parse_example(example_lines))
        }
    }

    /// Remove whitespace and leading '$'
    fn format_command(text: &str) -> String {
        text.split(' ')
            .filter(|chunk| !chunk.is_empty())
            .skip_while(|&chunk| chunk == "$")
            .join(" ")
    }

    fn parse_example(lines: Vec<String>) -> man::Example {
        let mut iter = lines.iter();
        let text = iter.next().unwrap();
        let command = iter.map(|line| format_command(line)).join("\n");
        Example::new().text(&text).command(&command).prompt("$")
    }

    manpage
}

// TODO get that from the clap::App?
fn add_help_subcommand(manpage: Manual) -> Manual {
    let help = Subcommand::new("help").description(
        "Prints this message or the help of the given subcommand(s)",
    );
    manpage.subcommand(help)
}

// TODO get that from the clap::App?
fn add_help_flag(manpage: Manual) -> Manual {
    let help = Flag::new()
        .short("-h")
        .long("--help")
        .help("Prints help information");
    manpage.flag(help)
}

// TODO get that from the clap::App?
fn add_version_flag(manpage: Manual) -> Manual {
    let version = Flag::new()
        .short("-V")
        .long("--version")
        .help("Prints version information");
    manpage.flag(version)
}

// TODO output to CARGO_TARGET_DIR
fn write_manpage(dir: &Path, manpage: Manual) -> anyhow::Result<()> {
    let name = dir.join(format!("{}.1", manpage.name.replace(" ", "-")));
    let mut file = File::create(name)?;
    file.write_all(manpage.render().as_bytes())?;
    Ok(())
}

fn date_today_iso() -> String {
    format!("{}", chrono::Utc::today().format("%Y-%m-%d"))
}
