use anyhow::anyhow;
use cargo_toml::{Manifest, Package};
use std::path::Path;

pub struct ManpageMaker<'a, 'b> {
    pub app: clap::App<'a, 'b>,
    pub metadata: Package,
    _package_name: Option<String>,
}

impl<'a, 'b> ManpageMaker<'a, 'b> {
    /// Create a new manpage maker.
    /// It holds the app and manifest to provide the data, and provides
    /// functions to create manpages.
    pub fn new(
        app: clap::App<'a, 'b>,
        manifest_path: &Path,
        package_name: Option<String>,
    ) -> anyhow::Result<Self> {
        let name = match package_name.clone() {
            Some(pn) => pn,
            None => app.p.meta.name.clone(),
        };
        let metadata = Self::read_metadata(manifest_path, &name)?;
        Ok(Self {
            app,
            metadata,
            _package_name: package_name,
        })
    }

    fn read_metadata(path: &Path, _name: &str) -> anyhow::Result<Package> {
        let manifest: Manifest = Manifest::from_slice(&std::fs::read(path)?)?;
        manifest
            .package
            .ok_or(anyhow!("no package found in manifest {:?}", path))
    }
}
