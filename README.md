## ManpageMaker for sequoia projects

Builds manpages for `sq`, `sqop`, `sqv` and `keyring-linter`, based on the clap cli definition and metadata from `Cargo.toml`.

Expects some files symlinked into the `src/` folder:
  - for sq:
    - `sq/sq_cli.rs` as `sq_cli.rs`
    - `sq/Cargo.toml` as `sq-Cargo.toml`
  - for sop:
    - `sequoia-sop/src/cli.rs` as `sop_cli.rs`
    - `sequoia-sop/Cargo.toml` as `sop-Cargo.toml`
    - `sequoia-sop/Cargo.lock` as `sop-Cargo.lock`
  - for sqv:
    - `sqv/sqv_cli.rs` as `sqv_cli.rs`
    - `sqv/Cargo.toml` as `sqv-Cargo.toml`
  - for keyring-linter:
    - `keyring-linter/src/cli.rs` as `keyring_linter_cli.rs`
    - `keyring-linter/Cargo.toml` as `keyring-linter-Cargo.toml`

There is a cargo feature for each app: `sq`, `sqop`,`keyring-linter`.

To run:
1. Add the symlinks for the project you want to build manpages for,
2. run `cargo run --features $toolname`
3. Find the manpages in the `man-$toolname` folder

### Beware, this is a hack!

Uses my fork of [`man`](https://crates.io/crates/man), with subcommands and other improvements and my fork of [`clap`](https://crates.io/crates/clap), to expose the needed app structure.
